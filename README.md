# todo_list

## TODOLIST

- [x] Setup Task interface, doJob() method
- [x] Build a CLI to interact with Task Manager
- [x] Task Manager need to list all tasks on list
- [x] Task Manager need to add task on list
- [x] Task Manager need to start task on list
- [x] Task Manager need to stop task on list
- [x] Task Manager need to pause task on list
- [x] Task Manager need remove task on list
- [x] Task Manager need to export task on list
- [x] Task Manager need to import task on list
- [x] Each of them run doJob() at the same time
- [] Task A can access data of Task B if it have demand

### Program explaination
- This program is a simple CLI. 
- User your terminal to run the program
- Choose from listed options
- After some tries, I decided to make the program run into 2 separate goroutines
- 1 Goroutine run the CLI so user can interact (add, pause, import,...task) with the program
- 1 Goroutine run the tasks (which user set status == running) every 15 seconds in the above goroutine
- I don't understand the final requirement from above TODOLIST